package com.example.demo.basic;

import java.io.PrintWriter;


import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.core.MyResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@ControllerAdvice
public class MyEerrorHandler {

    private static final Logger log = LoggerFactory.getLogger(MyEerrorHandler.class);
	
	@ExceptionHandler(Exception.class)
//	@ResponseBody
	public void handlerException(Exception e,PrintWriter w) {
		e.printStackTrace(w);
//		return e.getMessage();		
	}
	
	@ExceptionHandler(ClassNotFoundException.class)
	@ResponseBody
	public String handlerClassNotFoundException(ClassNotFoundException e) {
		return "<h1>类都找不到！开发你注意了，今年的年休你就别想了</h1>";	
	}
	
	@ExceptionHandler(ArrayIndexOutOfBoundsException.class)
	public String handlerArrayIndexOutOfBoundsException(ArrayIndexOutOfBoundsException e) {
		return "error/ArrayIndexOutOfBoundsException";	
	}
	
	@ExceptionHandler(BindException.class)
//	@ResponseBody
	public String handlerBindException(BindException e) {
	
//		FieldError fe = e.getFieldErrors().get(0);
//		return fe.getField()+":"+fe.getDefaultMessage();
		return "error/BindException";	
		
	}
	
//	@ExceptionHandler(MyResultException.class)
//	@ResponseBody
//	public String handlerMyResultException(MyResultException e) {
////		return e.getMessage();
//	
////		FieldError fe = e.getFieldErrors().get(0);
////		return fe.getField()+":"+fe.getDefaultMessage();
//		return "我的自定义异常:"+e.getMessage();	
//	}

	
	

}

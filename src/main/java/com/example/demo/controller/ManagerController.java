package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
@RequestMapping("manager")
public class ManagerController {

	@GetMapping(value="login")
	@ResponseBody
	public String login(HttpSession session) {
		session.setAttribute("user", "123234");
		return "登录成功"+session.getId();
	}
	
}

package com.example.demo.controller;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@Controller
@RequestMapping("user")
public class UserController {
	
	
	
	

	
//	@RequestMapping("abc")
//	@ResponseBody
//	public String helle(Date date) {
//		return date.toString();
//	}
	
	@Resource
	private UserService userService;
	
	@RequestMapping("all")
	public String findAll(Map<String, Object> map) {
		map.put("user", userService.findAll());
		return "user/all";
	}

	@RequestMapping("insert")
	public String insert(@Valid User user) {
			userService.insert(user);
			return "redirect:all";
	}
	
	@RequestMapping("insert_view")
	public String inserView() {
		return "user/insert";
	}
	
	@RequestMapping("delete")
	public String delete(@RequestParam(value="uId",required=true)Integer id) {
		userService.delete(id);
		return "redirect:all";
	}
	
	
	/**
	 * 修改之前查一次
	 * @param uId
	 * @param map
	 * @return
	 */
	@RequestMapping("update_view")
	public String updateView(Integer uId,Map<String, Object> map) {
		User user = userService.findOne(uId);
		map.put("user", user);
		return "user/update";
	}
	
	/**
	 * 修改
	 * @param user
	 * @return
	 */
	@RequestMapping("update")
	public String update(@Valid User user) {
		userService.update(user);
		return "redirect:all";
	}
	
	
}

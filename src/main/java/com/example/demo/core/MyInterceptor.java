package com.example.demo.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class MyInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO 3 写出到浏览器之前

	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO 2 controller 调之后 找到视图后 生成页面

	}

	// 页面跳转 ：1转发 2 重定向
	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object arg2) throws Exception {
		// TODO 1 controller 调之前
		 resp.setHeader("Content-type", "text/html;charset=UTF-8");
		HttpSession session = req.getSession();
		Object user = session.getAttribute("user");
//		resp.getWriter().println("<h1>the page must has loggin</h1>");
//		resp.getWriter().println("<script>alert('1秒之后跳百度');</script>");
		// // 2
		if(req.getRequestURI().contains("/manager/login")) {
			return true;
		}
		if(user==null&& !req.getRequestURI().contains("/manager/login"))
		 resp.sendRedirect("/manager/login");
		return user != null;
	}

}

package com.example.demo.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

//@Component
public class String2DateConvert implements Converter<String, Date> {

	@Resource
	private SimpleDateFormat simpleDateFormat;

	@Override
	public Date convert(String arg0) {
		try {
			return simpleDateFormat.parse(arg0);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return null;

	}

}

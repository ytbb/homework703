package com.example.demo.core;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


//@Aspect
//@Component
public class TestAop {
	
	private static Logger logger=LoggerFactory.getLogger(TestAop.class);

	@Pointcut("execution(* com.example.demo.controller..*.*(..))")
	public void controller() {};
	
	@AfterThrowing(value="controller()",throwing="e")
	public void doThrowing(Throwable e) {
		
		System.out.println("我将把这个错误信息写到日志:"+e.getMessage());
		
	}
	
	
	@Around("controller()")
	public void around(ProceedingJoinPoint pjp) {
		Object[] args = pjp.getArgs();
		try {
			if(args.length==1) {
				throw new MyResultException("参数都没有怎么访问系统!");
			}
			Object result = pjp.proceed(args);
		} catch (Throwable e) {
			logger.error("Controller Exception",e);
		}
	}
}

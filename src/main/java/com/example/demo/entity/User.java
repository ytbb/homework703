package com.example.demo.entity;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;





@Entity
public class User {

	@Id
	@GeneratedValue
	private Integer uId;
	@Pattern(regexp="[A-Z][A-Za-z_0-9]{5,15}",message="大写字母开始中间可以使下划线或者字符或者数字要求5-15位")
	private String name;
	@Range(min=18, max=89, message="用户年龄必须在18-89之间")    	
	private Integer age;
	private Integer sex;
	@DateTimeFormat(pattern="yyyy/MM/dd") 
	@Past(message = "日期必须在2011之前")  
	private Date date;
	@NotEmpty(message = "不能为空空字符串也不行")
	private String address;
	@Pattern(regexp="[A-Za-z]{0,15}",message="必须是英文，大小写都可以，15位")
	private String password;
	public Integer getuId() {
		return uId;
	}
	public void setuId(Integer uId) {
		this.uId = uId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}

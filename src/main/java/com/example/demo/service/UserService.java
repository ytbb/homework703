package com.example.demo.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {
	
	@Resource
	private UserRepository userRepository;

	@Transactional
	public User insert(User user) {
	 return	userRepository.save(user);
	}
	
	@Transactional
	public User update(User user) {
		return userRepository.save(user);
	}
	
	@Transactional
	public void delete(Integer id) {
		userRepository.delete(id);
	}
	
	public User findOne(Integer id) {
		return userRepository.findOne(id);
	}
	
	public List<User> findAll() {
		return userRepository.findAll();
	}
}

<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ page import="java.util.*" %>
    <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
<!-- 
	FieldError fe = e.getFieldErrors().get(0);
	return fe.getField()+":"+fe.getDefaultMessage(); 
	
	-->
<h1>你的参数验证失败</h1>
<p>
	具体错误消息如下
</p>
<table>
	<tr>
		<th>错误的字段名</th>
		<th>错误详细</th>
	</tr>
	<c:forEach items="${requestScope['javax.servlet.error.exception'].fieldErrors}" var="aa">
	<tr>
		<td>${aa.field }</td>
		<td>${aa.defaultMessage }</td>
	</tr>
	</c:forEach>
</table>

</body>
</html>
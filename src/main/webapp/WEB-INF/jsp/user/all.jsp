<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<table>

		<tr>
			<th>名字</th>
			<th>年龄</th>
			<th>性别</th>
			<th>地址</th>
			<th>密码</th>
			<th>日期</th>
			<th>编辑</th>
			
		</tr>
		<c:forEach items="${user}" var="u">

		<tr>
			<td>${u.name}</td>
			<td>${u.age}</td>
			<td>${u.sex==1?'男':'女'}</td>
			<td>${u.address}</td>
			<td>${u.password}</td>
			<td>${u.date}</td>
			<td><a href="insert_view">添加</a>|
			<a href="delete?uId=${u.uId}">删除</a>|
			<a href="update_view?uId=${u.uId}">修改</a>
			</td>
			
		</tr>
		</c:forEach>


	</table>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<form action="update" method="post">
		<div>
			<input type="hidden" name="uId" value="${user.uId }">
		</div>
		<div>
			名字:<input type="text" name="name" value="${user.name }">
		</div>
		<div>
			年龄:<input type="number" name="age" value="${user.age }">
		</div>
		<div>
			性别: 男<input type="radio" name="sex" value="1"
				${user.sex==1?'checked':''}> 女<input type="radio" name="sex"
				value="2" ${user.sex==2?'checked':''}>
		</div>
		<div>
			地址:<input type="text" name="address" value="${user.address }">
		</div>
		<div>
			密码:<input type="password" name="password" value="${user.password }">
		</div>
		<div>
			日期:<input type="text" name="date" value="${user.date }">
		</div>
		<input type="submit" value="提交">
	</form>

</body>
</html>